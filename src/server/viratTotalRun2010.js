const dataOfMatches = require("../data/matches.json");
const dataOfDeliveries = require("../data/deliveries.json");

function getseasonIDs(data) {
  if (data.season === "2010") return data["id"];
}

let seasonId = dataOfMatches
  .filter(getseasonIDs)
  .map((ids) => parseInt(ids.id));

// function viratBatting(data,id){
//   if (data.match_id === id ){
//     if(data.batsman === "V Kohli") return data["batsman_runs"];
//   }
// }
 

let viratScore = dataOfDeliveries
  .filter(data => seasonId.includes(parseInt(data.match_id)))
  .reduce((acc,delivery)=>{
    if(delivery.batsman==="V Kohli"){
      acc+=parseInt(delivery.batsman_runs);
    }
    return acc;
  },0);

  console.log(viratScore);