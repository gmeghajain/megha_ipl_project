//Find the number of times each team won the toss and also won the match.

//const dataOfMatches = require ("../data/matches.json") //to check output here

function tossWonMatchWon (dataOfMatches) {
    let team = {};
    if(Array.isArray(dataOfMatches)){
         team = dataOfMatches.filter((match) => match.winner === match.toss_winner)
        .reduce((accum,match)=>{
        (accum[match.toss_winner]===undefined) ? (accum[match.winner] = 0 ) : (accum[match.winner]+=1);
        return accum;
        }, {})
    } else {
        return {};
    }
    return team;
} 
module.exports = tossWonMatchWon;

//console.log(tossWonMatchWon(dataOfMatches)); // to check output here