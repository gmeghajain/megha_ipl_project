const fs = require('fs');

const dataOfMatches = require("../data/matches.json"); //variable to store data of matches
const dataOfDeliveries = require("../data/deliveries.json");//variable to store data of deliveries

//Problem functions exported
const matchesPlayedPerYear = require('./matchesPlayedPerYear');
const matchesWonPerTeam = require('./matchesWonPerTeam');
const extraRunPerTeam2016 = require('./extraRunPerTeam2016');
const topTenEconomicalBowlers2015 = require("./topTenEconomicalBowlers2015")
const extraRunsByBumrah2016 = require("./extraRunsByBumrah2016");
const tossWonMatchWon = require('./tossWonMatchWon');

// Problem1 : Number of matches played per year for all the years in IPL.
let seasonMatchCount = matchesPlayedPerYear(dataOfMatches);
console.log(seasonMatchCount);
fs.writeFileSync("../public/output/matchesPlayedPerYear.json",JSON.stringify(seasonMatchCount));

// Problem2 : Number of matches won per team per year in IPL.
let matchesWonCount = matchesWonPerTeam(dataOfMatches);
console.log(matchesWonCount);
fs.writeFileSync("../public/output/matchesWonPerTeam.json",JSON.stringify(matchesWonCount));

// Problem3 : Extra runs conceded per team in the year 2016
let extraRunCount = extraRunPerTeam2016(dataOfDeliveries,dataOfMatches);
console.log(extraRunCount);
fs.writeFileSync("../public/output/extraRunPerTeam2016.json",JSON.stringify(extraRunCount));

// Problem4 : Top 10 economical bowlers in the year 2015
let top10EcoBowler = topTenEconomicalBowlers2015(dataOfDeliveries,dataOfMatches);
console.log(top10EcoBowler);
fs.writeFileSync("../public/output/topTenEconomicalBowler2015.json",JSON.stringify(top10EcoBowler));

//newProblem: extra runs by Bumrah in year 2016
let extraByBumrah = extraRunsByBumrah2016(dataOfDeliveries);
console.log(extraByBumrah);
fs.writeFileSync("../public/output/extrasByBumrah2016.json",JSON.stringify(extraByBumrah));

// Problem5: //Find the number of times each team won the toss and also won the match.
let tossMatchWon = tossWonMatchWon(dataOfMatches);
console.log(tossMatchWon);
fs.writeFileSync("../public/output/tossWonMatchWon.json",JSON.stringify(tossMatchWon));

