//Find a player who has won the highest number of Player of the Match awards for each season
 const dataOfMatches = require("../data/matches.json") //to test
function playerOfTheMatch (dataOfMatches){
    if(!dataOfMatches) return {};
    const playerOfTheMatch = dataOfMatches.reduce((accum,match)=>{
        if(accum[match.season]===undefined) {
            accum[match.season] = {};
        }
        if(accum[match.season][match.payer_of_match]===undefined){
            accum[match.season][match.payer_of_match]=0;
        }
        accum[match.season][match.payer_of_match]+=1;
        return accum;
    },{});

    Object.keys(playerOfTheMatch).forEach((season)=>{
        const result = Object.entries(playerOfTheMatch[season])
        .sort((a,b)=> a[1]-b[1])
        .pop();
        playerOfTheMatch[season] = {
            [result[0]]: result[1] ,
        };
    });
    return playerOfTheMatch;
}

module.exports= playerOfTheMatch;

const ans = playerOfTheMatch(dataOfMatches); //to test
console.log(ans); //to test