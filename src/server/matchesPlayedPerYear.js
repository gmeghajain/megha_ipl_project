// Number of matches played per year for all the years in IPL.

//let testdata = require('../data/matches.json'); // to test function here

const { formatValueByType } = require("convert-csv-to-json");

/**
 * @param  {JSON} dataOfMatches stores data in json form
 * @param  {} matchCount stores match detail
 * @param  {} index iterate over array of objects
 */
function matchesPlayedPerYear (dataOfMatches) {

    if (!Array.isArray(dataOfMatches)) return {};
    let matchCount={};

    for(let index in dataOfMatches){
        if(matchCount[dataOfMatches[index].season]===undefined) matchCount[dataOfMatches[index].season]=0;
        else matchCount[dataOfMatches[index].season]+=1;
    }

    return matchCount;
}
//console.log(matchesPlayedPerYear(testdata)); // to test function here

module.exports = matchesPlayedPerYear;