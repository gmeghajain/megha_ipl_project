// Extra runs conceded per team in the year 2016
const { formatValueByType } = require("convert-csv-to-json");
//let testDataDelivery = require("../data/deliveries.json"); // to test
//let testDataMatch= require("../data/matches.json"); // to test
/**
 * @param  {} dataOfDeliveries contains deliveries data in json format
 * @param  {} dataOfMatches contains matches data in json format
 */
const extraRunPerTeam2016 = (dataOfDeliveries,dataOfMatches) => {
    if (!dataOfDeliveries && !dataOfMatches) return {};
    
    function getSeason16 (data){
        if(data.season==="2016"){
            return data['id'];
        }
    }
    
    let season16ids=dataOfMatches.filter(getSeason16).map(ids => parseInt(ids.id));
    
    /**
     * @param  {} data passed data to function
     * @param  {} extraRun store each extra run
     * @param  {} delivery stores delivery
     * @param  {} bowlingTeam stores name of bowling team
     */
    
    let extraRuns=dataOfDeliveries.filter(data => season16ids.includes(parseInt(data.match_id)))
    .reduce((extraRun,delivery)=>{
        let bowlingTeam=delivery["bowling_team"]
        if(extraRun[bowlingTeam]) extraRun[bowlingTeam]+=parseInt(delivery["extra_runs"]);
        else extraRun[bowlingTeam]=parseInt(delivery["extra_runs"]);
        return extraRun;
    },{});
   return extraRuns; 
}
//console.log(extraRunPerTeam2016(testDataDelivery,testDataMatch)); // to test
module.exports = extraRunPerTeam2016;

