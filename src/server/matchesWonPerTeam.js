// Number of matches won per team per year in IPL.
const { formatValueByType } = require("convert-csv-to-json");

//let testdata = require('../data/matches.json'); // to test function here
/**
 * @param  {} dataOfMatches stores matches data in json form
 */
const matchesWonPerTeam = (dataOfMatches) => {
    if (!Array.isArray(dataOfMatches)) return {};

    /**
     * @param  {} year stores season
     * @param  {} match stores match id
     */

    let matchWonYearWise = dataOfMatches.reduce((year,match) => {
        if(year[match.season]===undefined) year[match.season]={};

        if(year[match.season][match.winner]===undefined) year[match.season][match.winner]=1;
        else year[match.season][match.winner] += 1;

        return year;
    },{});

    return matchWonYearWise;
}
//console.log(matchesWonPerTeam(testdata)); // to test function here 

module.exports = matchesWonPerTeam;
