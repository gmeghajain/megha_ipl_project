// Top 10 economical bowlers in the year 2015
const { formatValueByType } = require("convert-csv-to-json");

// let testDataDelivery = require("../data/deliveries.json"); //to test 
// let testDataMatch= require("../data/matches.json"); // to test

function topTenEconomicalBowlers2015 (dataOfDeliveries,dataOfMatches){

    let ids=dataOfMatches.filter(obj => obj.season === '2015').map(obj => parseInt(obj.id));

    let deliveries2015 = dataOfDeliveries.filter(val => ids.includes(parseInt(val.match_id)));
    /**
     * @param  {} total accumulator
     * @param  {} delivery
     * @param  {} bowler name of bowler
     * @param  {} economyRate economy of bowler
     * @param  {} concededRun given extra runs
     * @param  {} bowls single delivery
     */
    let totalRunsBalls = deliveries2015.reduce((total, delivery)=>{
        let bowler = delivery.bowler;
        if (!total[bowler]) {
            total[bowler] = {};
            total[bowler].bowls = 1;
            total[bowler].concededRun = parseInt(delivery.total_runs);        
            total[bowler].economyRate = +((total[bowler].concededRun / ((total[bowler].bowls) / 6)).toFixed(3));        
        } 
        else {
            total[bowler].bowls += 1;
            total[bowler].concededRun += parseInt(delivery.total_runs);
            total[bowler].economyRate = +((total[bowler].concededRun / ((total[bowler].bowls) / 6)).toFixed(3));
        }
        return total;  
      }, {});

    const topTenEconomicalBowler = Object.entries(totalRunsBalls)
    .sort((a, b) => a[1].economyRate - b[1].economyRate)
    .slice(0, 10);

    let ans = Object.fromEntries(topTenEconomicalBowler);
    
    return ans;   
}
//console.log(topTenEconomicalBowlers2015(testDataDelivery,testDataMatch)) // to test
module.exports = topTenEconomicalBowlers2015;