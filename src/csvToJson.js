let csvToJson = require('convert-csv-to-json');

let matchesFileInputName = 'src/data/matches.csv'; //converting matches.csv to matches.json
let matchesFileOutputName = 'src/data/matches.json';

csvToJson.fieldDelimiter(',').generateJsonFileFromCsv(matchesFileInputName,matchesFileOutputName);

let deliveriesFileInputName = 'src/data/deliveries.csv'; //converting deliveries.csv to deliveries.json
let deliveriesFileOutputName = 'src/data/deliveries.json';

csvToJson.fieldDelimiter(',').generateJsonFileFromCsv(deliveriesFileInputName,deliveriesFileOutputName);